#include <LiquidCrystal.h>
#include <OneWire.h>
#include <DallasTemperature.h>

LiquidCrystal lcd(4, 6, 10, 11, 12, 13);
// Data wire is plugged into pin 2 on the Arduino
#define ONE_WIRE_BUS 2
// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);
// Pass our oneWire reference to Dallas Temperature. 
DallasTemperature sensors(&oneWire);


//add uptime to display. Use device runtime for greater precision
unsigned long newRuntime, oldRuntime;
unsigned int seconds, minutes, hours;


void setup() {
  sensors.begin();
  lcd.begin(16, 2);
  seconds = 0;
  minutes = 0;
  hours = 0;
  oldRuntime = millis();
}

void printRuntime(){
    seconds++;
    lcd.setCursor(0, 1);
    lcd.print("Up: ");
    
    
    if(minutes > 59){ //hours
      hours++;
      if(hours > 99) hours = 0; //overflow
      minutes = 0;
      lcd.setCursor(5, 1);
      lcd.print(hours);
      lcd.setCursor(7, 1);
      lcd.print("h");
    }

    if(seconds > 59){
      minutes++;
      seconds = 0;
      lcd.setCursor(8, 1);
      lcd.print(minutes);
      lcd.setCursor(10, 1);
      lcd.print("m");
    }

    if(seconds < 10){
      lcd.setCursor(11, 1);
      lcd.print(0);
      lcd.setCursor(12, 1);
      lcd.print(seconds);
    } else{
      lcd.setCursor(11, 1);
      lcd.print(seconds);
    }
    lcd.setCursor(13, 1);
    lcd.print("s");
    
    oldRuntime = newRuntime;
}

bool hasSecondPassed(){
  return (newRuntime - oldRuntime) >= 1000;
}

int msTillSecondPasses(){
  int msElapsed;
  msElapsed = newRuntime - oldRuntime;
  if(msElapsed > 1000) return 0;
  return 1000 - msElapsed;
}

void printTemperature(){
  sensors.requestTemperatures();
  float tempC, tempF;
  tempC = sensors.getTempCByIndex(0); //get ds18b20 temperature reading
  tempF = tempC * (9.0 / 5.0) + 32;
  lcd.setCursor(0,0);
  lcd.print(tempC);
  lcd.setCursor(6, 0);
  lcd.print("C");
  lcd.setCursor(8, 0);
  lcd.print(tempF);
  lcd.setCursor(15, 0);
  lcd.print("F");  
}

void loop() {
  

  //may no longer be needed
  if(hasSecondPassed()){
  printTemperature();
  printRuntime();
  }

  newRuntime = millis();
  //delay(msTillSecondPasses());
}
